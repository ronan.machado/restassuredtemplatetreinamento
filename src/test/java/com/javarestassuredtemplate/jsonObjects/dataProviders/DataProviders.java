package com.javarestassuredtemplate.jsonObjects.dataProviders;

import com.javarestassuredtemplate.jsonObjects.Login;
import org.testng.annotations.DataProvider;

public class DataProviders {

    @DataProvider(name = "dadosDeLoginErrados")
    public Object[] loginDataProvider(){
        //parametros corretos
        String email = "fulano@qa.com";
        String password = "teste";

        Login loginEmailInvalido = new Login();
        loginEmailInvalido.setEmail("fula@qa.com");
        loginEmailInvalido.setPassword(password);

        Login loginPasswordInvalido = new Login();
        loginPasswordInvalido.setEmail(email);
        loginPasswordInvalido.setPassword("test");

        return new Login[]{loginEmailInvalido, loginPasswordInvalido};
    }

    @DataProvider(name = "emailFormatoInvalido")
    public Object[][] emailFormatoInvalidoMassa(){
        return new String[][]{
                {"fulanoqa.com", "senha"},
                {"fulano@qacom", "senha"},
                {"fulano@.com", "senha"},
                {"fulanoqacom", "senha"},
                {"fulano", "senha"}
        };
    }
}
