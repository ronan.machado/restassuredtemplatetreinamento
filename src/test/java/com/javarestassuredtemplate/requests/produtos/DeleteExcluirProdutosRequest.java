package com.javarestassuredtemplate.requests.produtos;

import com.javarestassuredtemplate.bases.RequestRestBase;
import com.javarestassuredtemplate.enums.AuthenticationType;
import io.restassured.http.Method;

public class DeleteExcluirProdutosRequest extends RequestRestBase {
    public DeleteExcluirProdutosRequest(String id){
        requestService = "/produtos/" + id;
        method = Method.DELETE;
        authenticationType = AuthenticationType.OAUT2;
    }
}
