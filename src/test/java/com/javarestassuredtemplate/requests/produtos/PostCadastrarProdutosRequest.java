package com.javarestassuredtemplate.requests.produtos;

import com.javarestassuredtemplate.bases.RequestRestBase;
import com.javarestassuredtemplate.enums.AuthenticationType;
import io.restassured.http.Method;

public class PostCadastrarProdutosRequest extends RequestRestBase {
    public PostCadastrarProdutosRequest(){
        requestService = "/produtos";
        method = Method.POST;
        authenticationType = AuthenticationType.OAUT2;
        //pq não setar o header com o token aqui ????
    }


    public void setJsonBody(Object jsonObject){
        jsonBody = jsonObject;
    }
}
