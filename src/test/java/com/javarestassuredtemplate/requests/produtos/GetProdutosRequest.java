package com.javarestassuredtemplate.requests.produtos;

import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class GetProdutosRequest extends RequestRestBase {
    public GetProdutosRequest(String id){
        requestService = "/produtos";
        if(id != null){
            requestService += "/" + id;
        }
        method = Method.GET;
    }
}
