package com.javarestassuredtemplate.requests;

import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class LoginRequest extends RequestRestBase {
    public LoginRequest(){
        requestService = "/login";
        method = Method.POST;
    }

    public void SetJsonBody(Object jsonObject){
        jsonBody = jsonObject;
    }
}
