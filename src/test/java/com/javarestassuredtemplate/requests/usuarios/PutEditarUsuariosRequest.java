package com.javarestassuredtemplate.requests.usuarios;

import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class PutEditarUsuariosRequest extends RequestRestBase {
    public PutEditarUsuariosRequest(String id){
        requestService = "/usuarios/" + id;
        method = Method.PUT;
    }

    public void SetJsonBody(Object jsonObject){
        jsonBody = jsonObject;
    }
}
