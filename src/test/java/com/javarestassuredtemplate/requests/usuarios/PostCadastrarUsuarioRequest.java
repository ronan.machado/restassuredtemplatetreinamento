package com.javarestassuredtemplate.requests.usuarios;

import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class PostCadastrarUsuarioRequest extends RequestRestBase {
    public PostCadastrarUsuarioRequest(){
        requestService = "/usuarios";
        method = Method.POST;
    }

    public void SetJsonBody(Object jsonObject){
        jsonBody = jsonObject;
    }
}
