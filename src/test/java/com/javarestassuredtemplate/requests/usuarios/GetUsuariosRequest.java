package com.javarestassuredtemplate.requests.usuarios;

import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class GetUsuariosRequest extends RequestRestBase {
    public GetUsuariosRequest(String id){
        requestService = "/usuarios";
        if(id != null) {
            requestService += "/" + id;
        }
        method = Method.GET;
    }
}
