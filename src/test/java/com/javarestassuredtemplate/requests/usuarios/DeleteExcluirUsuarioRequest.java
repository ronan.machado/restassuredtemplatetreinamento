package com.javarestassuredtemplate.requests.usuarios;

import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class DeleteExcluirUsuarioRequest extends RequestRestBase {
    public DeleteExcluirUsuarioRequest(String id){
        requestService = "/usuarios/" + id;
        method = Method.DELETE;
    }
}
