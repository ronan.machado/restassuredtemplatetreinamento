package com.javarestassuredtemplate.requests.carrinhos;

import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class GetCarrinhosRequest extends RequestRestBase {
    public GetCarrinhosRequest(String id){
        requestService = "/carrinhos";
        if(id != null){
            requestService += "/" + id;
        }
        method = Method.GET;
    }
}
