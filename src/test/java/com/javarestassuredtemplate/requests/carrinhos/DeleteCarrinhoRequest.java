package com.javarestassuredtemplate.requests.carrinhos;

import com.javarestassuredtemplate.bases.RequestRestBase;
import com.javarestassuredtemplate.enums.AuthenticationType;
import io.restassured.http.Method;

public class DeleteCarrinhoRequest extends RequestRestBase {
    public DeleteCarrinhoRequest(){
        requestService = "/carrinhos/concluir-compra/";
        method = Method.DELETE;
        authenticationType = AuthenticationType.OAUT2;
    }
}
