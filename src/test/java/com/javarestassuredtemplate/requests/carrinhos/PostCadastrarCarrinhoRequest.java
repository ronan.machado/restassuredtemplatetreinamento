package com.javarestassuredtemplate.requests.carrinhos;

import com.javarestassuredtemplate.bases.RequestRestBase;
import com.javarestassuredtemplate.enums.AuthenticationType;
import io.restassured.http.Method;

public class PostCadastrarCarrinhoRequest extends RequestRestBase {
    public PostCadastrarCarrinhoRequest(){
        requestService = "/carrinhos";
        method = Method.POST;
        authenticationType = AuthenticationType.OAUT2;
    }

    public void SetJsonBody(Object jsonObject){
        jsonBody = jsonObject;
    }
}
