package com.javarestassuredtemplate.bases;

import com.javarestassuredtemplate.jsonObjects.User;
import com.javarestassuredtemplate.requests.usuarios.DeleteExcluirUsuarioRequest;
import com.javarestassuredtemplate.requests.usuarios.PostCadastrarUsuarioRequest;
import io.restassured.response.ValidatableResponse;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class UsuariosTestBase extends TestBase{
    protected String userId;
    protected String nome = "Ronan";
    protected String email = "ronan@teste.com";
    protected String password = "ronan";
    protected String administrador = "false";
    PostCadastrarUsuarioRequest postCadastrarUsuarioRequest;
    DeleteExcluirUsuarioRequest deleteExcluirUsuarioRequest;

    @BeforeMethod
    public void beforeMethod(){
        //Serialização java object
        User user = new User();
        user.setNome(nome);
        user.setEmail(email);
        user.setPassword(password);
        user.setAdministrador(administrador);

        //flow
        postCadastrarUsuarioRequest = new PostCadastrarUsuarioRequest();
        postCadastrarUsuarioRequest.SetJsonBody(user);
        ValidatableResponse response = postCadastrarUsuarioRequest.executeRequest();
        userId = response.extract().path("_id");
    }

    @AfterMethod
    public void afterMethod(){
        deleteExcluirUsuarioRequest = new DeleteExcluirUsuarioRequest(userId);
        deleteExcluirUsuarioRequest.executeRequest();
    }
}
