package com.javarestassuredtemplate.tests.contratos;

import com.javarestassuredtemplate.bases.UsuariosTestBase;
import com.javarestassuredtemplate.jsonObjects.User;
import com.javarestassuredtemplate.requests.usuarios.GetUsuariosRequest;
import com.javarestassuredtemplate.requests.usuarios.PutEditarUsuariosRequest;
import com.javarestassuredtemplate.utils.GeneralUtils;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import static io.restassured.module.jsv.JsonSchemaValidator.*;

public class ContratosDeUsuariosTest extends UsuariosTestBase {
    GetUsuariosRequest getUsuariosRequest;
    PutEditarUsuariosRequest putEditarUsuariosRequest;

    @Test
    public void contratoGetUsuarios(){
        //parametros
        int statusCodeEsperado = HttpStatus.SC_OK;
        String jsonSchema = GeneralUtils.readFileToAString("src/test/java/com/javarestassuredtemplate/jsons/usuarios/GetUsuariosSchema.json");

        //fluxo
        getUsuariosRequest = new GetUsuariosRequest(null);
        ValidatableResponse response = getUsuariosRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);
        response.assertThat().body(matchesJsonSchema(jsonSchema));
    }

    @Test
    public void contratoGetUsuarioPorId(){
        //parametros
        int statusCodeEsperado = HttpStatus.SC_OK;
        String jsonSchema = GeneralUtils.readFileToAString("src/test/java/com/javarestassuredtemplate/jsons/usuarios/GetUsuarioSchema.json");

        //flow
        getUsuariosRequest = new GetUsuariosRequest(userId);
        ValidatableResponse response = getUsuariosRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);
        response.assertThat().body(matchesJsonSchema(jsonSchema));
    }

    @Test
    public void contratoPutEditarUsuarios(){
        //parametros
        int statusCodeEsperado = HttpStatus.SC_OK;
        String jsonschema = GeneralUtils.readFileToAString("src/test/java/com/javarestassuredtemplate/jsons/usuarios/MessageSchema.json");

        //serialização java obeject
        User editedUser = new User();
        editedUser.setNome("Ronan Fernandes Machado");
        editedUser.setPassword(password);
        editedUser.setEmail(email);
        editedUser.setAdministrador(administrador);

        //flow
        putEditarUsuariosRequest = new PutEditarUsuariosRequest(userId);
        putEditarUsuariosRequest.SetJsonBody(editedUser);
        ValidatableResponse response = putEditarUsuariosRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);
        response.body(matchesJsonSchema(jsonschema));
    }
}
