package com.javarestassuredtemplate.tests.contratos;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.jsonObjects.User;
import com.javarestassuredtemplate.requests.usuarios.DeleteExcluirUsuarioRequest;
import com.javarestassuredtemplate.requests.usuarios.PostCadastrarUsuarioRequest;
import com.javarestassuredtemplate.utils.GeneralUtils;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class ContratoEcluirUsuarioTest extends TestBase {
    DeleteExcluirUsuarioRequest deleteExcluirUsuarioRequest;
    PostCadastrarUsuarioRequest postCadastrarUsuarioRequest;

    @Test
    public void contratoExcluirUsuario(){
        //parametros
        String userId;
        String nome = "Ronan";
        String email = "ronan@teste.com";
        String password = "ronan";
        String administrador = "false";
        String jsonEschema = GeneralUtils.readFileToAString("src/test/java/com/javarestassuredtemplate/jsons/usuarios/MessageSchema.json");
        int statusCodeesperado = HttpStatus.SC_OK;

        //Serialização java object
        User user = new User();
        user.setNome(nome);
        user.setEmail(email);
        user.setPassword(password);
        user.setAdministrador(administrador);

        //criação de novo usuario
        postCadastrarUsuarioRequest = new PostCadastrarUsuarioRequest();
        postCadastrarUsuarioRequest.SetJsonBody(user);
        ValidatableResponse responseNovoUsuario = postCadastrarUsuarioRequest.executeRequest();
        userId = responseNovoUsuario.extract().path("_id");

        //flow
        deleteExcluirUsuarioRequest = new DeleteExcluirUsuarioRequest(userId);
        ValidatableResponse response = deleteExcluirUsuarioRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeesperado);
        response.body(matchesJsonSchema(jsonEschema));
    }
}
