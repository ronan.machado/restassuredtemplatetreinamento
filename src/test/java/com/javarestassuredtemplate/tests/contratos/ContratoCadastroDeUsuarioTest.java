package com.javarestassuredtemplate.tests.contratos;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.jsonObjects.User;
import com.javarestassuredtemplate.requests.usuarios.DeleteExcluirUsuarioRequest;
import com.javarestassuredtemplate.requests.usuarios.PostCadastrarUsuarioRequest;
import com.javarestassuredtemplate.utils.GeneralUtils;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import static io.restassured.module.jsv.JsonSchemaValidator.*;

public class ContratoCadastroDeUsuarioTest extends TestBase {
    PostCadastrarUsuarioRequest postCadastrarUsuarioRequest;
    DeleteExcluirUsuarioRequest deleteExcluirUsuarioRequest;

    @Test
    public void contratoPostUsuario(){
        //parametros
        String userId;
        String nome = "Ronan";
        String email = "ronan@teste.com";
        String password = "ronan";
        String administrador = "false";
        int statusCodeesperado = HttpStatus.SC_CREATED;
        String jsonObejct = GeneralUtils.readFileToAString("src/test/java/com/javarestassuredtemplate/jsons/usuarios/PostUsuarioSchema.json");

        //Serialização java object
        User user = new User();
        user.setNome(nome);
        user.setEmail(email);
        user.setPassword(password);
        user.setAdministrador(administrador);

        //flow
        postCadastrarUsuarioRequest = new PostCadastrarUsuarioRequest();
        postCadastrarUsuarioRequest.SetJsonBody(user);
        ValidatableResponse response = postCadastrarUsuarioRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeesperado);
        userId = response.body(matchesJsonSchema(jsonObejct)).extract().path("_id");

        //teardown
        deleteExcluirUsuarioRequest = new DeleteExcluirUsuarioRequest(userId);
        deleteExcluirUsuarioRequest.executeRequest();
    }
}
