package com.javarestassuredtemplate.tests;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.jsonObjects.Login;
import com.javarestassuredtemplate.jsonObjects.dataProviders.DataProviders;
import com.javarestassuredtemplate.requests.LoginRequest;
import com.javarestassuredtemplate.utils.GeneralUtils;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;
import static io.restassured.module.jsv.JsonSchemaValidator.*;
import java.lang.String;

public class LoginTests extends TestBase {
    LoginRequest loginRequest;

    @Test
    public void contratoDeLogin(){
        String email = "fulano@qa.com";
        String password = "teste";
        int statusCodeEsperado = HttpStatus.SC_OK;
        String jsonSchema = GeneralUtils.readFileToAString("src/test/java/com/javarestassuredtemplate/jsons/LoginSchema.json");

        //serialização java object
        Login login = new Login();
        login.setEmail(email);
        login.setPassword(password);


        //fluxo
        loginRequest = new LoginRequest();
        loginRequest.SetJsonBody(login);
        ValidatableResponse response = loginRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);
        response.assertThat().body(matchesJsonSchema(jsonSchema));
    }

    @Test
    public void realizarLoginComSucesso(){
        //parametros
        String email = "fulano@qa.com";
        String password = "teste";
        int statusCodeEsperado = HttpStatus.SC_OK;

        //serialização java object
        Login login = new Login();
        login.setEmail(email);
        login.setPassword(password);


        //fluxo
        loginRequest = new LoginRequest();
        loginRequest.SetJsonBody(login);
        ValidatableResponse response = loginRequest.executeRequest();

        //Assesrções
        response.statusCode(statusCodeEsperado);
        response.body("message", equalTo("Login realizado com sucesso"),
                "authorization", isA(String.class));
    }

    @Test(dataProvider = "dadosDeLoginErrados", dataProviderClass = DataProviders.class)
    public void loginComDadosErrados(Login loginData){
        //parametros
        int statusCodeEsperado = HttpStatus.SC_UNAUTHORIZED;

        loginRequest = new LoginRequest();
        loginRequest.SetJsonBody(loginData);
        ValidatableResponse response = loginRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);
        response.body("message", equalTo("Email e/ou senha inválidos"));
    }

    @Test(dataProvider = "emailFormatoInvalido", dataProviderClass = DataProviders.class)
    public void loginComEmailInvalido(String email, String password){
        //parametros
        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;

        //Serialização java object
        Login login = new Login();
        login.setEmail(email);
        login.setPassword(password);

        loginRequest = new LoginRequest();
        loginRequest.SetJsonBody(login);
        ValidatableResponse response = loginRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);
        response.body("email", equalTo("email deve ser um email válido"));

    }
}
