package com.javarestassuredtemplate.tests.fluxos;

import com.javarestassuredtemplate.GlobalParameters;
import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.jsonObjects.*;
import com.javarestassuredtemplate.requests.LoginRequest;
import com.javarestassuredtemplate.requests.carrinhos.DeleteCarrinhoRequest;
import com.javarestassuredtemplate.requests.carrinhos.GetCarrinhosRequest;
import com.javarestassuredtemplate.requests.carrinhos.PostCadastrarCarrinhoRequest;
import com.javarestassuredtemplate.requests.produtos.DeleteExcluirProdutosRequest;
import com.javarestassuredtemplate.requests.produtos.GetProdutosRequest;
import com.javarestassuredtemplate.requests.produtos.PostCadastrarProdutosRequest;
import com.javarestassuredtemplate.requests.usuarios.DeleteExcluirUsuarioRequest;
import com.javarestassuredtemplate.requests.usuarios.PostCadastrarUsuarioRequest;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;

public class FazerUmaCompraTests extends TestBase {
    PostCadastrarProdutosRequest postCadastrarProdutosRequest;
    LoginRequest loginRequest;
    PostCadastrarUsuarioRequest postCadastrarUsuarioRequest;
    GetProdutosRequest getProdutosRequest;
    PostCadastrarCarrinhoRequest postCadastrarCarrinhoRequest;
    GetCarrinhosRequest getCarrinhosRequest;
    DeleteCarrinhoRequest deleteCarrinhoRequest;
    DeleteExcluirProdutosRequest deleteExcluirProdutosRequest;
    DeleteExcluirUsuarioRequest deleteExcluirUsuarioRequest;

    private String adminLogin(){
        //parametros
        String email = "fulano@qa.com";
        String password = "teste";

        //serialização java object
        Login login = new Login();
        login.setEmail(email);
        login.setPassword(password);


        //fluxo
        loginRequest = new LoginRequest();
        loginRequest.SetJsonBody(login);
        ValidatableResponse response = loginRequest.executeRequest();

        response.statusCode(HttpStatus.SC_OK);
        return response.extract().path("authorization");
    }

    private String createUser(){
        String nome = "Ronan";
        String email = "ronan@teste.com";
        String password = "ronan";
        String administrador = "false";

        User user = new User();
        user.setNome(nome);
        user.setEmail(email);
        user.setPassword(password);
        user.setAdministrador(administrador);

        postCadastrarUsuarioRequest = new PostCadastrarUsuarioRequest();
        postCadastrarUsuarioRequest.SetJsonBody(user);
        ValidatableResponse response = postCadastrarUsuarioRequest.executeRequest();

        response.statusCode(HttpStatus.SC_CREATED);
        return response.extract().path("_id");
    }

    private String[] loginUser(){
        String email = "ronan@teste.com";
        String password = "ronan";
        String userId = createUser();

        Login login = new Login();
        login.setEmail(email);
        login.setPassword(password);

        //fluxo
        loginRequest = new LoginRequest();
        loginRequest.SetJsonBody(login);
        ValidatableResponse response = loginRequest.executeRequest();

        response.statusCode(HttpStatus.SC_OK);

        return new String[]{userId, response.extract().path("authorization")};
    }

    private String[] createProduct(){
        //paramteros
        String nome = "Mouse";
        String preco = "100";
        String descricao = "Mouse bom";
        String quantidade = "5";
        String adminToken = adminLogin();

        //serialização objeto
        Product product = new Product();
        product.setNome(nome);
        product.setPreco(preco);
        product.setDescricao(descricao);
        product.setQuantidade(quantidade);

        //flow
        postCadastrarProdutosRequest = new PostCadastrarProdutosRequest();
        postCadastrarProdutosRequest.setJsonBody(product);
        GlobalParameters.setToken(adminToken);
        ValidatableResponse response = postCadastrarProdutosRequest.executeRequest();

        response.statusCode(HttpStatus.SC_CREATED);
        return new String[]{response.extract().path("_id"), adminToken};
    }

    @Test
    public void fazerUmaCompraComSucesso(){
        //setup
        String[] createProductInfo = createProduct();
        String productId = createProductInfo[0];
        String adminToken = createProductInfo[1];
        String[] userInfo = loginUser();

        //parametros
        String userId = userInfo[0];
        String userToken = userInfo[1];
        Carrinho carrinho;
        ProdutoNoCarrinho produtoNoCarrinho;
        int quantidadeASerComprada = 2;

        //fluxo
        //listar todos os produtos
        getProdutosRequest = new GetProdutosRequest(null);
        ValidatableResponse responseProdutos = getProdutosRequest.executeRequest();
        responseProdutos.statusCode(HttpStatus.SC_OK);
        String idProdutoParaCompra = responseProdutos.extract().path("produtos[2]._id");

        //consultar detalhes do produto
        getProdutosRequest = new GetProdutosRequest(idProdutoParaCompra);
        ValidatableResponse responseProduto = getProdutosRequest.executeRequest();
        responseProduto.statusCode(HttpStatus.SC_OK);
        int quantidadeDeProdutoAntesDaCompra = responseProduto.extract().path("quantidade");

        //colocar produto no carrinho
        carrinho = new Carrinho();
        produtoNoCarrinho = new ProdutoNoCarrinho();
        produtoNoCarrinho.setIdProduto(idProdutoParaCompra);
        produtoNoCarrinho.setQuantidade(quantidadeASerComprada);
        carrinho.setProdutos(new Object[]{produtoNoCarrinho});
        postCadastrarCarrinhoRequest = new PostCadastrarCarrinhoRequest();
        postCadastrarCarrinhoRequest.SetJsonBody(carrinho);
        GlobalParameters.setToken(userToken);
        ValidatableResponse carrinhoResponse = postCadastrarCarrinhoRequest.executeRequest();
        carrinhoResponse.statusCode(HttpStatus.SC_CREATED);
        String carrinhoId = carrinhoResponse.extract().path("_id");

        //visualizar carrinho
        getCarrinhosRequest = new GetCarrinhosRequest(carrinhoId);
        ValidatableResponse carrinhoViewResponse = getCarrinhosRequest.executeRequest();
        carrinhoViewResponse.statusCode(HttpStatus.SC_OK);

        //concluir compra
        deleteCarrinhoRequest = new DeleteCarrinhoRequest();
        ValidatableResponse concluirCompraResponse = deleteCarrinhoRequest.executeRequest();
        getProdutosRequest = new GetProdutosRequest(idProdutoParaCompra);
        ValidatableResponse produtoComprado = getProdutosRequest.executeRequest();
        produtoComprado.statusCode(HttpStatus.SC_OK);

        //Asserções
        concluirCompraResponse.statusCode(HttpStatus.SC_OK);
        concluirCompraResponse.body("message", equalTo("Registro excluído com sucesso"));
        produtoComprado.body("quantidade", equalTo(quantidadeDeProdutoAntesDaCompra - quantidadeASerComprada));

        //tear down
        //excluir usuario e produto criados
        deleteExcluirUsuarioRequest = new DeleteExcluirUsuarioRequest(userId);
        deleteExcluirUsuarioRequest.executeRequest().statusCode(HttpStatus.SC_OK);

        deleteExcluirProdutosRequest = new DeleteExcluirProdutosRequest(productId);
        GlobalParameters.setToken(adminToken);
        deleteExcluirProdutosRequest.executeRequest().statusCode(HttpStatus.SC_OK);
    }
}
